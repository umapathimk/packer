# vm.pkr.hcl

variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {}
variable "instance_type" {
  default = "t2.micro"
}
variable "ami_name" {}

source "amazon-ebs" "example" {
  ami_name      = var.ami_name
  instance_type = var.instance_type
  region        = var.aws_region
  access_key    = var.aws_access_key
  secret_key    = var.aws_secret_key

  # Other configurations...
}

# Other Packer configurations...
